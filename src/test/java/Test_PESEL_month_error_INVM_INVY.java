import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.get;

public class Test_PESEL_month_error_INVM_INVY {

    @Test
    public void testPESEL_MonthIncorrect00() {
        Response response = get("/Pesel?pesel=06001563687");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"06001563687\",\"isValid\":false,\"dateOfBirth\":null,\"gender\":\"Female\",\"errors\":[{\"errorCode\":\"INVY\",\"errorMessage\":\"Invalid year.\"},{\"errorCode\":\"INVM\",\"errorMessage\":\"Invalid month.\"}]}";

        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void testPESEL_MonthYearMismatch() {
        Response response = get("/Pesel?pesel=06561563688");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"06561563688\",\"isValid\":false,\"dateOfBirth\":null,\"gender\":\"Female\",\"errors\":[{\"errorCode\":\"INVY\",\"errorMessage\":\"Invalid year.\"},{\"errorCode\":\"INVM\",\"errorMessage\":\"Invalid month.\"}]}";

        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void testPESEL_MonthIncorrect14() {
        Response response = get("/Pesel?pesel=06141563684");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"06141563684\",\"isValid\":false,\"dateOfBirth\":\"2015-06-14T00:00:00\",\"gender\":\"Female\",\"errors\":[{\"errorCode\":\"INVY\",\"errorMessage\":\"Invalid year.\"},{\"errorCode\":\"INVM\",\"errorMessage\":\"Invalid month.\"}]}";

        Assert.assertEquals(actualBody, expectedBody);
    }
}
