import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.get;

public class Test_PESEL_characters_error_NBRQ {

    @Test
    public void testPESEL_NonNumericLetter() {
        Response response = get("/Pesel?pesel=0606156368n");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"0606156368n\",\"isValid\":false,\"dateOfBirth\":null,\"gender\":null,\"errors\":[{\"errorCode\":\"NBRQ\",\"errorMessage\":\"Invalid characters. Pesel should be a number.\"}]}";

        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void testPESEL_NonNumericSpecial() {
        Response response = get("/Pesel?pesel=0606156368/");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"0606156368/\",\"isValid\":false,\"dateOfBirth\":null,\"gender\":null,\"errors\":[{\"errorCode\":\"NBRQ\",\"errorMessage\":\"Invalid characters. Pesel should be a number.\"}]}";

        Assert.assertEquals(actualBody, expectedBody);
    }
}
