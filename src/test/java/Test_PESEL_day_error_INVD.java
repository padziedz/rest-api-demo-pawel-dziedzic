import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;

public class Test_PESEL_day_error_INVD {

    @Test
    public void testPESEL_DayIncorrectDay0() {
        Response response = get("/Pesel?pesel=06060063689");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"06060063689\",\"isValid\":false,\"dateOfBirth\":null,\"gender\":\"Female\",\"errors\":[{\"errorCode\":\"INVD\",\"errorMessage\":\"Invalid day.\"}]}";

        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void testPESEL_DayIncorrectDay32nd() {
        Response response = get("/Pesel?pesel=00703290566");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"00703290566\",\"isValid\":false,\"dateOfBirth\":null,\"gender\":\"Female\",\"errors\":[{\"errorCode\":\"INVD\",\"errorMessage\":\"Invalid day.\"}]}";

        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void testPESEL_DayLeapYearIncorrectFeb29th() {
        Response response = get("/Pesel?pesel=83022999149");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"83022999149\",\"isValid\":false,\"dateOfBirth\":null,\"gender\":\"Female\",\"errors\":[{\"errorCode\":\"INVD\",\"errorMessage\":\"Invalid day.\"}]}";

        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void testPESEL_DayLeapYearCorrectFeb28th() {
        Response response = get("/Pesel?pesel=83022899142");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"83022899142\",\"isValid\":true,\"dateOfBirth\":\"1983-02-28T00:00:00\",\"gender\":\"Female\",\"errors\":[]}";

        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void testPESEL_DayLeapYearCorrectFeb29th() {
        Response response = get("/Pesel?pesel=16222995064");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"16222995064\",\"isValid\":true,\"dateOfBirth\":\"2016-02-29T00:00:00\",\"gender\":\"Female\",\"errors\":[]}";

        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void testPESEL_DayLeapYearIncorrectFeb30th() {
        Response response = get("/Pesel?pesel=16223095060");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"16223095060\",\"isValid\":false,\"dateOfBirth\":null,\"gender\":\"Female\",\"errors\":[{\"errorCode\":\"INVD\",\"errorMessage\":\"Invalid day.\"}]}";

        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void testPESEL_DayIncorrectApr31st() {
        Response response = get("/Pesel?pesel=83043136860");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"83043136860\",\"isValid\":false,\"dateOfBirth\":null,\"gender\":\"Female\",\"errors\":[{\"errorCode\":\"INVD\",\"errorMessage\":\"Invalid day.\"}]}";

        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void testPESEL_DayIncorrectJun31st() {
        Response response = get("/Pesel?pesel=25063181718");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"25063181718\",\"isValid\":false,\"dateOfBirth\":null,\"gender\":\"Male\",\"errors\":[{\"errorCode\":\"INVD\",\"errorMessage\":\"Invalid day.\"}]}";

        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void testPESEL_DayIncorrectSep31st() {
        Response response = get("/Pesel?pesel=54063167312");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"54063167312\",\"isValid\":false,\"dateOfBirth\":null,\"gender\":\"Male\",\"errors\":[{\"errorCode\":\"INVD\",\"errorMessage\":\"Invalid day.\"}]}";

        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void testPESEL_DayIncorrectNov31st() {
        Response response = get("/Pesel?pesel=00913113084");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"00913113084\",\"isValid\":false,\"dateOfBirth\":null,\"gender\":\"Female\",\"errors\":[{\"errorCode\":\"INVD\",\"errorMessage\":\"Invalid day.\"}]}";

        Assert.assertEquals(actualBody, expectedBody);
    }

    //    This test is to test leap year 'additional' rules i.e. if a year is divisible by 100 then it IS NOT a leap year
    //    and if it IS divisible by 400 then it IS a leap year
    @Test
    public void testPESEL_DayIncorrectFeb29th1900dividedby100() {
        Response response = get("/Pesel?pesel=00022925967");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"00022925967\",\"isValid\":false,\"dateOfBirth\":null,\"gender\":\"Female\",\"errors\":[{\"errorCode\":\"INVD\",\"errorMessage\":\"Invalid day.\"}]}";

        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void testPESEL_DayLeapYearCorrectFeb29th2000dividedBy400() {
        Response response = get("/Pesel?pesel=00222966614");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"00222966614\",\"isValid\":true,\"dateOfBirth\":\"2000-02-29T00:00:00\",\"gender\":\"Male\",\"errors\":[]}";

        Assert.assertEquals(actualBody, expectedBody);
    }
}
