import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.get;

public class Test_PESEL_length_error_INVL {

    @Test
    public void testPESEL_TooShort() {
        Response response = get("/Pesel?pesel=0606156368");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"0606156368\",\"isValid\":false,\"dateOfBirth\":null,\"gender\":null,\"errors\":[{\"errorCode\":\"INVL\",\"errorMessage\":\"Invalid length. Pesel should have exactly 11 digits.\"}]}";

        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void testPESEL_TooLong() {
        Response response = get("/Pesel?pesel=060615636834");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"060615636834\",\"isValid\":false,\"dateOfBirth\":null,\"gender\":null,\"errors\":[{\"errorCode\":\"INVL\",\"errorMessage\":\"Invalid length. Pesel should have exactly 11 digits.\"}]}";

        Assert.assertEquals(actualBody, expectedBody);
    }
}
