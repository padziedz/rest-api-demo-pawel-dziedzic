import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.get;

public class Test_GET_Statuscodes extends BaseTest {

    @Test
    public void testPESEL_StatusCode200() {
        Response response = get("/Pesel?pesel=06061563683");

        Assert.assertEquals(response.statusCode(), 200);
    }

    @Test
    public void testPESEL_StatusCode400() {
        Response response = get("/Pesel?pesel=");

        Assert.assertEquals(response.statusCode(), 400);
    }
}
