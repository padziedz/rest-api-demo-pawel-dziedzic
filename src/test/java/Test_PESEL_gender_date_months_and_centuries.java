import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.get;

public class Test_PESEL_gender_date_months_and_centuries {

    @Test(dataProvider = "testCasesForMalePESEL")
    public void testPESEL_MalePESEL(String pesel, String expectedMessage) {
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + pesel);
        String actualBody = response.getBody().asString();

        Assert.assertEquals(actualBody,expectedMessage);
    }

//    Test to check all centuries are valid for male PESELs
    @DataProvider
    public Object[][] testCasesForMalePESEL() {
        return new Object[][]{
                {"91820101712", "{\"pesel\":\"91820101712\",\"isValid\":true,\"dateOfBirth\":\"1891-02-01T00:00:00\",\"gender\":\"Male\",\"errors\":[]}"},
                {"91053121936", "{\"pesel\":\"91053121936\",\"isValid\":true,\"dateOfBirth\":\"1991-05-31T00:00:00\",\"gender\":\"Male\",\"errors\":[]}"},
                {"99213191256", "{\"pesel\":\"99213191256\",\"isValid\":true,\"dateOfBirth\":\"2099-01-31T00:00:00\",\"gender\":\"Male\",\"errors\":[]}"},
                {"60501211877", "{\"pesel\":\"60501211877\",\"isValid\":true,\"dateOfBirth\":\"2160-10-12T00:00:00\",\"gender\":\"Male\",\"errors\":[]}"},
                {"00610141399", "{\"pesel\":\"00610141399\",\"isValid\":true,\"dateOfBirth\":\"2200-01-01T00:00:00\",\"gender\":\"Male\",\"errors\":[]}"},
                {"63873113778", "{\"pesel\":\"63873113778\",\"isValid\":true,\"dateOfBirth\":\"1863-07-31T00:00:00\",\"gender\":\"Male\",\"errors\":[]}"},
                {"53283154553", "{\"pesel\":\"53283154553\",\"isValid\":true,\"dateOfBirth\":\"2053-08-31T00:00:00\",\"gender\":\"Male\",\"errors\":[]}"},
                {"75913083734", "{\"pesel\":\"75913083734\",\"isValid\":true,\"dateOfBirth\":\"1875-11-30T00:00:00\",\"gender\":\"Male\",\"errors\":[]}"},
        };
    }

    @Test(dataProvider = "testCasesForFemalePESEL")
    public void testPESEL_FemalePESEL(String pesel, String expectedMessage) {
        Response response = get("https://peselvalidatorapitest.azurewebsites.net/api/Pesel?pesel=" + pesel);
        String actualBody = response.getBody().asString();

        Assert.assertEquals(actualBody,expectedMessage);
    }

    //    Test to check all centuries are valid for female PESELs
    @DataProvider
    public Object[][] testCasesForFemalePESEL() {
        return new Object[][]{
                {"87841654825", "{\"pesel\":\"87841654825\",\"isValid\":true,\"dateOfBirth\":\"1887-04-16T00:00:00\",\"gender\":\"Female\",\"errors\":[]}"},
                {"83103159547", "{\"pesel\":\"83103159547\",\"isValid\":true,\"dateOfBirth\":\"1983-10-31T00:00:00\",\"gender\":\"Female\",\"errors\":[]}"},
                {"07253121568", "{\"pesel\":\"07253121568\",\"isValid\":true,\"dateOfBirth\":\"2007-05-31T00:00:00\",\"gender\":\"Female\",\"errors\":[]}"},
                {"87481699688", "{\"pesel\":\"87481699688\",\"isValid\":true,\"dateOfBirth\":\"2187-08-16T00:00:00\",\"gender\":\"Female\",\"errors\":[]}"},
                {"99723128203", "{\"pesel\":\"99723128203\",\"isValid\":true,\"dateOfBirth\":\"2299-12-31T00:00:00\",\"gender\":\"Female\",\"errors\":[]}"},
                {"71033164047", "{\"pesel\":\"71033164047\",\"isValid\":true,\"dateOfBirth\":\"1971-03-31T00:00:00\",\"gender\":\"Female\",\"errors\":[]}"},
                {"31443018022", "{\"pesel\":\"31443018022\",\"isValid\":true,\"dateOfBirth\":\"2131-04-30T00:00:00\",\"gender\":\"Female\",\"errors\":[]}"},
                {"63663040787", "{\"pesel\":\"63663040787\",\"isValid\":true,\"dateOfBirth\":\"2263-06-30T00:00:00\",\"gender\":\"Female\",\"errors\":[]}"},
                {"50293025800", "{\"pesel\":\"50293025800\",\"isValid\":true,\"dateOfBirth\":\"2050-09-30T00:00:00\",\"gender\":\"Female\",\"errors\":[]}"}
        };
    }
}
