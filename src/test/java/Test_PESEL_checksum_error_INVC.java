import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.get;

public class Test_PESEL_checksum_error_INVC {

    @Test
    public void testPESEL_ChecksumIncorrect() {

        Response response = get("/Pesel?pesel=06061563682");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"06061563682\",\"isValid\":false,\"dateOfBirth\":\"1906-06-15T00:00:00\",\"gender\":\"Female\",\"errors\":[{\"errorCode\":\"INVC\",\"errorMessage\":\"Check sum is invalid. Check last digit.\"}]}";

        Assert.assertEquals(actualBody, expectedBody);
    }
}
