import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.get;

public class Test_PublicApi_Example {

    @Test
    public void testEvilInsult_StatusCode200() {
        Response response = get("https://evilinsult.com/generate_insult.php?lang=en&type=json");

        Assert.assertEquals(response.statusCode(), 200);

        String actualInsult = response.path("insult");
        System.out.println("===============================================");
        System.out.println("Random insult for your pleasure: ");
        System.out.println(actualInsult);
        System.out.println("===============================================");
    }
}
